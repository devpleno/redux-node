const redux = require('redux')

function contadorReducer(state = 0, action) {
    switch (action.type) {
        case 'INCREMENT':
            return state + action.value;
        case 'DECREMENT':
            return state - action.value;
    }

    console.log('action', action)
    return state
}

const store = redux.createStore(contadorReducer)

function storeAlterado() {
    console.log('storeState', store.getState())
}

store.subscribe(storeAlterado)

const incrementar = { type: 'INCREMENT', value: 3 }
const decrementar = { type: 'DECREMENT', value: 2 }

store.dispatch(incrementar)

store.dispatch(decrementar)

